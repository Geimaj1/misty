#!/bin/bash

set -x
set -e

{
    ENV_FILE=${CI_PROJECT_DIR}/face/misty-face/.env

    touch $ENV_FILE
    chmod 600 $ENV_FILE

    echo "AWS_COGNITO_POOL_ID=${AWS_COGNITO_POOL_ID}" > $ENV_FILE
    echo "AWS_IOT_ENDPOINT=${AWS_IOT_ENDPOINT}" >> $ENV_FILE
    echo "AWS_REGION=${AWS_REGION}" >> $ENV_FILE


} &> /dev/null