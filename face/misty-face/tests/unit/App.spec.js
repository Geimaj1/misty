//libs
import Vue from "vue";
import Vuetify from "vuetify";

//components
import App from "@/App";
import router from "@/router/router";

// Utilities
import { mount, createLocalVue } from "@vue/test-utils";
import { wrap } from "module";

// const localVue = createLocalVue();

// Vue.use(vuetify);

describe("App.vue", () => {
	let vuetify;

	beforeEach(() => {
		Vue.use(Vuetify);
		vuetify = new Vuetify({
			mocks: {
				$vuetify: {
					lang: {
						t: val => val
					},
					theme: {
						dark: true
					}
				}
			}
		});
	});

	it("should have a custom title", () => {
		const wrapper = mount(App, {
			Vue,
			vuetify,
			router,
			propsData: {
				title: "Misty Roots"
			}
		});

		const title = wrapper.find(".v-toolbar__title");
		expect(title.text()).toBe("Misty Roots");
	});
});
