const AWS = require("aws-sdk");
const AWSConfiguration = require("./aws-config.js");
const AWSIoTData = require("aws-iot-device-sdk");

AWS.config.region = AWSConfiguration.region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
	IdentityPoolId: AWSConfiguration.poolId
});

const shadows = AWSIoTData.thingShadow({
	region: AWS.config.region,
	host: AWSConfiguration.host,
	clientId: "Misty roots browser" + Math.floor(Math.random() * 100000 + 1),
	protocol: "wss",
	maximumReconnectTimeMs: 8000,
	debug: false,
	accessKeyId: "",
	secretKey: "",
	sessionToken: ""
});

const iot = new AWS.Iot();

// shadows.on("error", e => {
// 	//   console.log("error", e);
// });

shadows.on("status", function(name, statusType, clientToken, stateObject) {
	if (statusType === "rejected") {
		if (stateObject.code !== 404) {
			console.log("resync with thing shadow");
			var opClientToken = shadows.get(name);
			if (opClientToken === null) {
				console.log("operation in progress");
			}
		}
	}
});

const cognitoIdentity = new AWS.CognitoIdentity();
AWS.config.credentials.get(function(err, data) {
	if (!err) {
		data;
		console.log("retrieved identity: " + AWS.config.credentials.identityId);
		var params = {
			IdentityId: AWS.config.credentials.identityId
		};
		cognitoIdentity.getCredentialsForIdentity(params, function(err, data) {
			if (!err) {
				//
				// Update our latest AWS credentials; the MQTT client will use these
				// during its next reconnect attempt.
				//
				shadows.updateWebSocketCredentials(
					data.Credentials.AccessKeyId,
					data.Credentials.SecretKey,
					data.Credentials.SessionToken
				);
			} else {
				console.log("error retrieving credentials: " + err);
				console.log("error retrieving credentials: " + err);
			}
		});
	} else {
		console.log("error retrieving identity:" + err);
		console.log("error retrieving identity: " + err);
	}
});

function getIotClient() {
	return shadows;
}

function listThingGroups() {
	return new Promise((res, rej) => {
		iot.listThingGroups((err, data) => {
			if (err !== null) {
				rej(err);
			} else {
				res(data);
			}
		});
	});
}

function listThingsInThingGroup(params) {
	return new Promise((res, rej) => {
		iot.listThingsInThingGroup(params, (err, data) => {
			awsPromiseHandler(err, data, res, rej);
		});
	});
}

function describeThingGroup(params) {
	return new Promise((res, rej) => {
		iot.describeThingGroup(params, (err, data) =>
			awsPromiseHandler(err, data, res, rej)
		);
	});
}

function createThingGroup(params) {
	return new Promise((resolve, reject) => {
		iot.createThingGroup(params, (err, data) =>
			awsPromiseHandler(err, data, resolve, reject)
		);
	});
}

function createThing(params) {
	return new Promise((res, rej) => {
		iot.createThing(params, (err, data) =>
			awsPromiseHandler(err, data, res, rej)
		);
	});
}

function addThingToGroup(params) {
	console.log("adding to group");

	return new Promise((res, rej) => {
		iot.addThingToThingGroup(params, (err, data) => {
			console.log("added to group");
			awsPromiseHandler(err, data, res, rej);
		});
	});
}

function listThingTypes() {
	return new Promise((resolve, reject) => {
		iot.listThingTypes((err, data) =>
			awsPromiseHandler(err, data, resolve, reject)
		);
	});
}

function createThingType(params) {
	return new Promise((resolve, reject) => {
		iot.createThingType(params, (err, data) =>
			awsPromiseHandler(err, data, resolve, reject)
		);
	});
}

function promiseAwsFunction(awsFunction, params) {
	return new Promise((resolve, reject) => {
		awsFunction(params, (err, data) =>
			awsPromiseHandler(err, data, resolve, reject)
		);
	});
}

function awsPromiseHandler(err, data, resolve, reject) {
	if (err !== null) {
		reject(err);
	} else {
		resolve(data);
	}
}

function awsFunctionPromiseWrapper(awsFunction, params) {
	return new Promise((resolve, reject) => {
		awsFunction(params, (err, data) => {
			if (err !== null) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	});
}

export default {
	shadows,
	listThingGroups,
	listThingsInThingGroup,
	createThingGroup,
	listThingTypes,
	createThingType,
	createThing,
	addThingToGroup,
	describeThingGroup
};
