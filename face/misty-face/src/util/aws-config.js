const awsConfiguration = {
	poolId: process.env.AWS_COGNITO_POOL_ID, // 'YourCognitoIdentityPoolId'
	host: process.env.AWS_IOT_ENDPOINT, // 'YourAWSIoTEndpoint', e.g. 'prefix.iot.us-east-1.amazonaws.com'
	region: process.env.AWS_REGION // 'YourAwsRegion', e.g. 'us-east-1'
};
module.exports = awsConfiguration;
