import Dash from "@/components/Dash";
import Operations from "@/components/Operations";
import Operation from "@/components/Operation";
import Settings from "@/components/Settings";
import Types from "@/components/Types";

export default [
	{
		path: "/",
		name: "Dash",
		component: Dash,
		icon: "mdi-view-dashboard"
	},
	{
		path: "/ops",
		name: "Operations",
		component: Operations,
		icon: "mdi-view-dashboard"
	},
	{
		path: "/ops/:id",
		name: "Operation",
		component: Operation,
		icon: "mdi-view-dashboard"
	},
	{
		path: "/types",
		name: "Types",
		component: Types,
		icon: "mdi-tank"
	},
	{
		path: "/settings",
		name: "Settings",
		component: Settings,
		icon: "mdi-settings"
	}
];
