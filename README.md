# Requirements

## ESP8525 Firmware:

Sonoff Tasmota

## Server:

-   Docker
    -   Time seriers DB
    -   MQTT broker
    -   Web Server

-- Build ideas

-   Grow op
    -   board
        -   mqtt topic
        -   name
        -   sensor
            -   type
            -   pin / mqtt command
    -   mode (auto / warn / manual)
    -   rules
    -   timers
